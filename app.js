'use strict';

const fs = require('fs');
const readline = require('readline');
const rp = require('request-promise');

console.log("Starting Up...");

function openReader(dev, pat) {
	console.log('Opening reader device: ', dev);
	var readStream = fs.createReadStream(dev);
	if (!readStream)
		return console.log('create readstream failed');

	var rl = readline.createInterface({
		input: readStream,
		output: process.stdout,
		terminal: false
	});
	if (!rl)
		return console.log('create interface failed');


	rl.on('line', function (line) {
		console.log("Got line:", line);
		var r = new RegExp(pat, 'g');
		var match = r.exec(line);

		if (!match) {
			return console.log("unmatched line:", line);
		}
		console.log('URI', process.env.CGMEM_ENDPOINT)
		rp({
			uri: process.env.CGMEM_ENDPOINT,
			method: 'POST',
			json: true,
			headers: {
				'X-Kiosk-Authorization': (process.env.CGMEM_TOKEN || 'ABCD')
			},
			body: {
				accessId: match[1]
			}
		})
			.then(() => {
				console.log('Good checkin');
				return;
			})
			.catch(reason => {
				console.log('Failed:', reason.statusCode);
				if (reason.statusCode == 404) {
					console.log('Member not found or not registered');
					return;
				}

				if (reason.statusCode == 403)
					console.log('Server authentication failed');
				else
					console.log('Unknown server response:', reason);
				return;
			})
		return;
	})

	rl.on('close', function () {
		console.log("EOF received on input.  Attempting to reopen...");
		openReader(dev, pat);
	});
}

openReader(process.env.CGMEM_READER_DEVICE || '/dev/tty', '(.+)');


