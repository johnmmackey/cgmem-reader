# Installation
```
sudo cp cg-reader.service /lib/systemd/system/cg-reader.service
sudo systemctl daemon-reload
sudo systemctl stop getty@tty1.service
sudo systemctl enable cg-reader.service
sudo reboot
```